package inf112.skeleton.app;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;

import inf112.variant1.app.Gator;

/**
 * Unit tests (example).
 * 
 * (Run using `mvn test`)
 */
public class ExampleTest {
	/**
	 * Static method run before everything else
	 */
	@BeforeAll
	static void setUpBeforeAll() {
		// Legg til dette hvis du vil kunne bruke (deler av) libGDX i testene
		
		HeadlessApplicationConfiguration config = new HeadlessApplicationConfiguration();
		ApplicationListener listener = new ApplicationAdapter() {
		};
		new HeadlessApplication(listener, config);
	}

	/**
	 * Setup method called before each of the test methods
	 */
	@BeforeEach
	void setUpBeforeEach() {
	}

	/**
	 * Simple test case
	 */
	@Test
	void dummy1() {
 
	}

    @Test
	void testMove() {
//        var gator = new Gator(null, 25, 25);
//        gator.setVelocity(100, 50);
//        gator.act(1);
//        assertEquals(100, gator.getX());
//        assertEquals(50, gator.getY());
    }
	/**
	 * Simple test case
	 */
	@Test
	void dummy2() {
		// For floats and doubles it's best to use assertEquals with a delta, since
		// floating-point numbers are imprecise
		float a = 100000;
		a = a + 0.1f;
		assertEquals(100000.1, a, 0.01);
	}

	/**
	 * Parameterized test case, reading arguments from comma-separated strings
	 * 
	 * @param a
	 * @param b
	 * @param c
	 */
	@CsvSource(value = { "1,1,2", "1,2,3", "2,3,5", "3,5,8", "5,8,13", "8,13,21" })
	@ParameterizedTest(name = "{0}+{1} == {2}")
	void addTest(int a, int b, int c) {
		assertEquals(c, a + b);
	}
}