package inf112.skeleton.app;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;
import static inf112.codeexamples.Möckitö.*;
import inf112.variant2.model.ModelObject;

/**
 * Tests for our “how is mocking implemented” demo system, Möckitö.
 */
public class MöckitöTest {
	/**
	 * Static method run before everything else
	 */
	@BeforeAll
	static void setUpBeforeAll() {
	}

	/**
	 * Setup method called before each of the test methods
	 */
	@BeforeEach
	void setUpBeforeEach() {
	}

	@Test
	void möckTestList() {
		// vi får advarsel fordi vi ikke kan spesifisere elementtypen
		// når vi mocker (Java støtter ikke “List<String>.class” e.l.)
		@SuppressWarnings("unchecked")
		List<String> mockedList = (List<String>) mock(List.class);

		mockedList.add("mango"); // fungerer tilsynelatende, men gjør ingenting

		when(mockedList.size()).thenReturn(1);
		when(mockedList.contains("mango")).thenReturn(true);
		when(mockedList.contains("banana")).thenReturn(false);

		assertEquals(1, mockedList.size()); // ok, vi har programmert size() til å returnere 1
		assertTrue(mockedList.contains("mango")); // også ok
		assertFalse(mockedList.contains("banana")); // også ok siden vi sa at mockedList.contains("banana") == false
		assertFalse(mockedList.contains("apple")); // også ok siden false er default

		verify(mockedList).add("mango"); // ok, vi kalte denne
		assertThrows(AssertionError.class, //
				() -> verify(mockedList).add("banana")); // skal feile
	}

	@Test
	void möckTestModelObject() {
		ModelObject obj = mock(ModelObject.class);
		obj.move(2f, 4f);
		
		when(obj.x()).thenReturn(5f);
		when(obj.y()).thenReturn(3f);
		assertEquals(5f, obj.x());
		assertEquals(3f, obj.y());

		obj.move(5f, 2f);
		verify(obj).move(5f, 2f);
	}

	@Test
	void möckTestSimple() {
		@SuppressWarnings("unchecked")
		List<String> mockedList = mock(List.class);
		
		mockedList.add("mango"); // fungerer tilsynelatende, men gjør ingenting
		when(mockedList.size()).thenReturn(1);
		when(mockedList.contains("mango")).thenReturn(true);
		when(mockedList.contains("banana")).thenReturn(false);
		assertEquals(1, mockedList.size()); // ok, vi har programmert size() til å returnere 1
		assertTrue(mockedList.contains("mango")); // også ok
		assertFalse(mockedList.contains("banana")); // også ok siden vi sa at mockedList.contains("banana") == false
		assertFalse(mockedList.contains("apple")); // også ok siden false er default
	}

	@Test
	void möckTestAddActors() {
		@SuppressWarnings("unchecked")
		List<ModelObject> actors = mock(List.class);

		var gator = mock(ModelObject.class);
		var fish = mock(ModelObject.class);
		when(fish.width()).thenReturn(5f);
		when(fish.height()).thenReturn(5f);
		when(fish.x()).thenReturn(5f);

		actors.add(fish);
		actors.add(gator);

		verify(actors).add(gator);
		verify(actors).add(fish);
	}

}