package inf112.variant2.model;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.math.Vector2;

import inf112.util.Calculations;
import inf112.variant2.view.Viewable;

abstract class AbstractModelImpl implements Viewable, ModelObject {
	protected Vector2 position = new Vector2();
	protected Vector2 velocity = new Vector2();
	protected Vector2 size = new Vector2();
	protected String skin;

	public AbstractModelImpl(double w, double h, String skin) {
		size.x = (float) w;
		size.y = (float) h;
		this.skin = skin;
	}

	@Override
	public ModelObject act(float deltaTime, World world) {
		var move = new Vector2(velocity).scl(deltaTime);
		var outside = Calculations.checkBorder(position, move, new Vector2(1920, 1080));

		if (outside.x != 0f)
			velocity.scl(-1, 1);
		if (outside.y != 0f)
			velocity.scl(1, -1);
		move.set(velocity).scl(deltaTime);
		position.add(move);
		return this;
	}

	@Override
	public String skin() {
		return skin;
	}

	@Override
	public float x() {
		return position.x;
	}

	@Override
	public float y() {
		return position.y;
	}

	@Override
	public float direction() {
		return velocity.angleDeg();
	}

	@Override
	public String state() {
		return "standing";
	}

	@Override
	public ModelObject accelerate(double ddx, double ddy) {
		return accelerateTo(velocity.x + ddx, velocity.y + ddy);
	}

	@Override
	public ModelObject accelerateTo(double dx, double dy) {
		velocity.set((float) dx, (float) dy);

		return this;
	}

	@Override
	public float width() {
		return size.x;
	}

	@Override
	public float height() {
		return size.y;
	}

	@Override
	public float speed() {
		return velocity.len();
	}

	@Override
	public ModelObject moveTo(double x, double y) {
		position.set((float) x, (float) y);
		return this;
	}

	@Override
	public ModelObject moveTo(Vector2 dest) {
		position.set(dest);
		return this;
	}
	
	public Vector2 getPosition() {
		Map<String,String> map = new HashMap<>();
		
		for(Map.Entry<String,String> e : map.entrySet()) {
			System.out.println(e.getKey() + ":" + e.getValue());
		}
		return position;
	}

	@Override
	public ModelObject move(double dx, double dy) {
		position.add((float) dx, (float) dy);
		return this;
	}

	@Override
	public Vector2 velocity() {
		return new Vector2(velocity);
	}

	@Override
	public Vector2 velocity(Vector2 dest) {
		return dest.set(velocity);
	}
}
