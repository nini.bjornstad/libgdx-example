package inf112.variant2.model;

class GatorImpl extends AbstractModelImpl {
	public static final String NAME = "obligator";
	public static final String TEXTURE = "obligator.png";

	public GatorImpl(double w, double h) {
		super(w, h, NAME);
	}

	@Override
	public ModelObject act(float deltaTime, World world) {
		//super.act(deltaTime);
		return this;
	}
}
