package inf112.variant2.model;

import com.badlogic.gdx.math.Vector2;

import inf112.variant2.view.Viewable;

/**
 * @author anya
 *
 */
public interface ModelObject extends Viewable {

	/**
	 * Perform one time step
	 * 
	 * @param deltaTime Elapsed time since last update (in seconds)
	 * @return TODO
	 */
	ModelObject act(float deltaTime, World world);

	/**
	 * How this ModelObject should be drawn
	 */
	String skin();

	/**
	 * @return X coordinate
	 */
	float x();

	/**
	 * @return X coordinate
	 */
	float y();

	/**
	 * @return Current direction, as an angle (in degrees)
	 */
	float direction();

	/**
	 * @return Width of object
	 */
	float width();

	/**
	 * @return Height of object
	 */
	float height();

	/**
	 * @return Speed of object (length of velocity vector) in pixels/second
	 */
	float speed();

	/**
	 * @return A copy of the velocity vector
	 */
	Vector2 velocity();

	/**
	 * Write velocity to a vector and return
	 * 
	 * @param dest Destination vector
	 * @return dest
	 */
	Vector2 velocity(Vector2 dest);

	/**
	 * 
	 * @return "walking", "jumping", "standing"
	 */
	String state();

	/**
	 * Set position
	 * 
	 * @param x new X coordinate
	 * @param y new Y coordinate
	 * @return this
	 */
	ModelObject moveTo(double x, double y);

	/**
	 * Set position
	 * 
	 * @param newPos new (x,y) coordinates
	 * @return this
	 */
	ModelObject moveTo(Vector2 newPos);

	/**
	 * Change position
	 * 
	 * @param dx value to add to X
	 * @param dy value to add to Y
	 * @return this
	 */
	ModelObject move(double dx, double dy);

	/**
	 * Set velocity
	 * 
	 * @param dx new horizontal speed
	 * @param dy new vertical speed
	 * @return this
	 */
	ModelObject accelerateTo(double dx, double dy);

	/**
	 * Change acceleration
	 * 
	 * @param ddx horizontal change
	 * @param ddy vertical change
	 * @return this
	 */
	ModelObject accelerate(double ddx, double ddy);
}