package inf112.variant2.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import com.badlogic.gdx.Gdx;

import inf112.util.PluginLoader;

/**
 * This factory produces <code>ModelObject</code>s.
 * 
 * @author anya
 *
 */
public interface ModelFactory {

	static Map<String, BiFunction<Double, Double, ModelObject>> factories = new HashMap<>();

	// This is a straight-forward way to set up the factory, though we should
	// maybe put this code together with the application startup.
	public static void simpleInitialize() {
		register("pipp", (w,h) -> new PippImpl(w,h)); 
		register("obligator", (w,h) -> new GatorImpl(w,h)); 
	}

	// Fancy initialization – will automatically load and register all classes
	// that implement the ModelObject interface.
	public static void autoInitialize() {
		//ModelFactory.class.getResourceAsStream("/birds/pipp.png");
		PluginLoader.loadClasses(ModelFactory.class, "..", ModelObject.class).forEach(c -> {
			System.out.println(c.getName());
			String name = PluginLoader.getConstant(c, "NAME", String.class);
			if (name == null)
				name = c.getSimpleName().replaceAll("Impl$", "");
			BiFunction<Double, Double, ModelObject> factory = PluginLoader.makeFactory(c, Double.TYPE, Double.TYPE);
			if (factory != null) {
				factories.put(name, factory);
				Gdx.app.log("ModelFactory", String.format("Loaded ‘%s’ (from %s)", name, c.getName()));
			}
		});
		;
	}
	
	public static ModelObject create(String kind, double size) {
		return create(kind, size, -1);
	}

	public static ModelObject create(String kind, double w, double h) {
		BiFunction<Double, Double, ModelObject> modelFactory = factories.get(kind);
		if (modelFactory != null) {
			return modelFactory.apply(w, h);
		}
		throw new RuntimeException("Don't know how to make " + kind);
	}

	public static List<String> kinds() {
		return List.copyOf(factories.keySet());
	}


	public static void register(String name, ModelFactory fact) {
		factories.put(name, (a, b) -> fact.create(a, b));
	}
	
	public ModelObject create(double x, double y);
}
