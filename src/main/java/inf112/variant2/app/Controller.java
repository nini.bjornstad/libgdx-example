package inf112.variant2.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import inf112.variant2.model.ModelFactory;
import inf112.variant2.model.ModelObject;
import inf112.variant2.model.World;
import inf112.variant2.view.View;

public class Controller implements ApplicationListener {
	private static final int SENSITIVITY = 100;
	private Sound bellSound;
	private View view;
	private Random random = new Random();
	private Viewport viewport;

	// probably better to move this to the model package and call it World or
	// something
	private List<ModelObject> models = new ArrayList<>();
	private ModelObject player;
	private BitmapFont font;

	@Override
	public void create() {
		// Called at startup
		font = new BitmapFont(Gdx.files.internal("fonts/garamond.fnt"));
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		ModelFactory.autoInitialize();
		Gdx.app.log("MainController", "hello");
		viewport = new FitViewport(1920, 1080);

		bellSound = Gdx.audio.newSound(Gdx.files.internal("blipp.ogg"));
		view = new View();
		view.loadTextures(ModelFactory::kinds);

		var listener = new InputAdapter() {
			@Override
			public boolean keyDown(int keyCode) {
				if (keyCode == Keys.LEFT || keyCode == Keys.A) {
					System.out.println("left");
					player.accelerate(-10, 0);
					return true;
				} else if (keyCode == Keys.RIGHT || keyCode == Keys.D) {
					System.out.println("right");
					player.accelerate(10, 0);
					return true;
				} else if (keyCode == Keys.ESCAPE) {
					Gdx.app.exit();
				}
				return false;

			}
		};
		var multiplexer = new InputMultiplexer(listener);
		Gdx.input.setInputProcessor(multiplexer);

		loadScene("level1");

		Gdx.graphics.setForegroundFPS(60);
	}

	private Vector2 randomPosition() {
		return new Vector2((.25f + random.nextFloat() / 2) * viewport.getWorldWidth(),
				(.25f + random.nextFloat() / 2) * viewport.getWorldHeight());
	}

	private float randomSpeed() {
		return 500 * (random.nextFloat() - .5f); // random.nextGaussian()*100
	}

	private void loadScene(String scene) {
		var objectKinds = ModelFactory.kinds();

		player = ModelFactory.create("obligator", 200);
		player.moveTo(randomPosition());

		// since we create stuff based on names, we could just as easily load the
		// level from disk
		if (scene.equals("level1")) {
			for (int i = 0; i < 50; i++) {
				var kind = objectKinds.get(random.nextInt(objectKinds.size()));
				var model = ModelFactory.create(kind, random.nextDouble(50, 150));
				model.moveTo(randomPosition()).accelerateTo(randomSpeed(), randomSpeed());
				 models.add(model);
			}
		}

		models.add(player);
	}

	@Override
	public void dispose() {
		// Called at shutdown

		// Graphics and sound resources aren't managed by Java's garbage collector, so
		// they must generally be disposed of manually when no longer needed. But,
		// any remaining resources are typically cleaned up automatically when the
		// application exits, so these aren't strictly necessary here.
		// (We might need to do something like this when loading a new game level in
		// a large game, for instance, or if the user switches to another application
		// temporarily (e.g., incoming phone call on a phone, or something).

		bellSound.dispose();
	}

	@Override
	public void render() {
		float delta = Gdx.graphics.getDeltaTime();

		processInputState(delta);
		act(delta);
		view.drawFrame(models);
		SpriteBatch batch = new SpriteBatch();
		batch.begin();
		font.setColor(Color.BLACK);
		font.draw(batch, "Hellojkdfskfhajklhsdkfhaslja", 1920 / 2, 1080 / 2, 200, Align.left, true);
		batch.end();
	}

	private void processInputState(float deltaTime) {
		var amount = SENSITIVITY * deltaTime;

		// for these keys, we want something to happen *while they are pressed*,
		// so we use Gdx.input.isKeyPressed() rather than a listener.
		//
		// Some keys (like left/right and up/down) conflict with each other, so
		// it would perhaps be better to track KeyDown and KeyUp, and pick
		// the last pressed one in case of a conflict.
		if (Gdx.input.isKeyPressed(Keys.A) || Gdx.input.isKeyPressed(Keys.LEFT))
			player.accelerate(-amount, 0);
		else if (Gdx.input.isKeyPressed(Keys.D) || Gdx.input.isKeyPressed(Keys.RIGHT))
			player.accelerate(amount, 0);
		else if (Gdx.input.isKeyPressed(Keys.W) || Gdx.input.isKeyPressed(Keys.UP))
			player.accelerate(0, amount);
		else if (Gdx.input.isKeyPressed(Keys.S) || Gdx.input.isKeyPressed(Keys.DOWN))
			player.accelerate(-0, -amount);

	}

	public void act(float delta) {
		// probably better to have a World (or Stage) model rather
		// than working with a list in the controller
		World world = new World() { // TODO: move to models package
			@Override
			public List<ModelObject> objects() {
				return Collections.unmodifiableList(models);
			}
		};
		
		for (var o : models) {
			o.act(delta, world);
		}

		// Alternative approach: every change create a new immutable object,
		// and we get a new world (model list) for every turn:
		//
		// models = models.stream().map(obj -> obj.act(delta, world)).toList();
	}

	@Override
	public void resize(int width, int height) {
		// Called whenever the window is resized (including with its original site at
		// startup)

		viewport.update(width, height, true);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
