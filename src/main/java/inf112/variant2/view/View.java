package inf112.variant2.view;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.ScreenUtils;

public class View {
	private Map<String, Texture> textures = new HashMap<>();
	private SpriteBatch batch;

	public View() {
		this.batch = new SpriteBatch();
	}

	public void loadTextures(Scene s) {
		for (var tex : textures.values()) {
			tex.dispose();
		}
		for (var texName : s.requiredTextures()) {
			textures.put(texName, new Texture(Gdx.files.internal(texName + ".png")));
		}
	}

	public void drawFrame(List<? extends Viewable> objects) {
		// Start with a blank screen
		ScreenUtils.clear(Color.WHITE);

		batch.begin();

		for (var o : objects) {
			drawObject(o);
		}
		batch.end();
	}

	private void drawObject(Viewable viewObj) {
		var skin = viewObj.skin();

		var texture = textures.get(skin);
		if (texture != null) {
			float x = viewObj.x(), y =  viewObj.y();
			float w = viewObj.width(), h = viewObj.height();
			int tw = texture.getWidth(), th = texture.getHeight();
			float r = viewObj.direction();
			if(h == -1)
					h = (w*th)/tw;
					
			
			batch.draw(texture,x-w/2f, y-h/2f, // position
					w/2,h/2, // origin
					w, h, // size
					1, 1, // scale
					r, // rotation
					0, 0, // texture offset
					tw, th, // texture size
					false, r > 90 && r < 270); // flip
		}
	}
}
