package inf112.variant2.view;

public interface Viewable {
    float x();
    float y();
    float direction();
    float width();
    float height();
    String skin();
    
}
