package inf112.util;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;

public class Calculations {
	public static float border = .01f;
	private static final Vector2 tmp = new Vector2();

	/**
	 * Calculate how far past the stage border an actor is.
	 * 
	 * Adding the result to the actor's position will place it exactly at the edge
	 * if it was outside. To be exact:
	 * <ul>
	 * <li>If result.x < 0, left edge is result.x pixels outside the left border
	 * <li>If result.x > 0, right edge is -result.x pixels outside the right border
	 * <li>If result.y < 0, bottom edge is result.y pixels outside the bottom border
	 * <li>If result.y > 0, top edge is result.y pixels outside the top border
	 * </ul>
	 * <p>
	 * The result will be (0,0) if the actor was already inside the border.
	 * <p>
	 * <b>Precondition:</b> Actor's width/height must be no larger than the stage.
	 * 
	 * @param actor An actor
	 * @return (x,y) to add to position to place actor at edge or inside of border
	 */
	public static Vector2 checkBorder(Actor actor) {
		return checkBorder(actor, new Vector2(), 0);
	}

	/**
	 * Calculate how far past the stage border an actor will move in one time step
	 * 
	 * The x,y components of the result vector will be positive if the actor moves
	 * past the left/bottom border, and negative if it moves past the right/top
	 * border. Moving the actor, and the adding the result vector to its position
	 * will move the actor back inside if it went past.
	 * <p>
	 * To be exact
	 * <ul>
	 * <li>If result.x < 0, left edge would be result.x pixels outside the left
	 * border
	 * <li>If result.x > 0, right edge would be -result.x pixels outside the right
	 * border
	 * <li>If result.y < 0, bottom edge would be result.y pixels outside the bottom
	 * border
	 * <li>If result.y > 0, top edge would be result.y pixels outside the top border
	 * </ul>
	 * <p>
	 * The result will be (0,0) if the move would keep the actor inside.
	 * <p>
	 * <b>Precondition:</b> Actor's width/height must be no larger than the stage.
	 * 
	 * @param actor     The actor
	 * @param direction Actor's current direction
	 * @param dist      Move distance this time step (speed*deltaTime)
	 * @return How far the actor would have to move to get back inside the stage
	 */
	public static Vector2 checkBorder(Actor actor, Vector2 direction, float dist) {
		float W = actor.getStage().getWidth(), H = actor.getStage().getHeight();
		float borderX = W * border, borderY = H * border;

		// relative movement this turn
		var move = new Vector2(direction).scl(dist);

		// find lower left and upper right corner (in stage coordinates
		var corner0 = actor.getParent().localToStageCoordinates(new Vector2(actor.getX(Align.bottomLeft) + move.x, //
				actor.getY(Align.bottomLeft) + move.y));
		var corner1 = actor.getParent().localToStageCoordinates(tmp.set(actor.getX(Align.topRight) + move.x, //
				actor.getY(Align.topRight) + move.y));
		// calculate size in stage coordinates
		var w = Math.abs(corner1.x - corner0.x);
		var h = Math.abs(corner1.y - corner0.y);

		// our math will be wrong if we're larger than the stage
		assert w <= W && h <= H;

		// calculate how far we'll be outside the corner of the screen border
		// (in pixels – divide by border to get a fraction
		corner0.sub(borderX, borderY);
		corner0.x = Math.max(0, -corner0.x); //
		corner0.y = Math.max(0, -corner0.y); //
		corner1.sub(W - borderX, H - borderY);
		corner1.x = Math.min(0, -corner1.x); //
		corner1.y = Math.min(0, -corner1.y); //

		// one of them should be 0, so we can combine by adding
		corner0.add(corner1);
		return corner0;
	}
	
	public static Vector2 checkBorder(Vector2 position, Vector2 move, Vector2 bounds) {
		float W = bounds.x, H = bounds.y;
		float borderX = W * border *0, borderY = H * border*0;

		// find lower left and upper right corner (in stage coordinates
		var newPos0 = new Vector2(position).add(move);
		var newPos1 = new Vector2(newPos0);

		// calculate how far we'll be outside the corner of the screen border
		// (in pixels – divide by border to get a fraction
		newPos0.sub(borderX, borderY);
		newPos0.x = Math.max(0, -newPos0.x); //
		newPos0.y = Math.max(0, -newPos0.y); //
		newPos1.sub(W - borderX, H - borderY);
		newPos1.x = Math.min(0, -newPos1.x); //
		newPos1.y = Math.min(0, -newPos1.y); //

		// one of them should be 0, so we can combine by adding
		newPos0.add(newPos1);
		return newPos0;
	}
}
