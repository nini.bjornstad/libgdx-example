package inf112.flocking;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;

public class Bird extends Actor {
	private static final float MIN_SPEED = 100;
	private static final float MAX_SPEED = 1000;
	private static final float BORDER = .1f;
	private Texture texture;
	private Vector2 direction = new Vector2(1, 0);
	private float speed = 100;
	private float size2;
	private float avoidThreshold, sightLimit;

	public Bird(Texture tex, float width, float height) {
		texture = tex;
		setBounds(0, 0, width, height);
		setOrigin(Align.center);
		setPosition(0, 0, Align.bottomLeft);
		setScale(1);
		width = getScaleX() * getWidth() / 2;
		height = getScaleY() * getHeight() / 2;
		size2 = (width * width + height * height);
		avoidThreshold = 5 * size2;
		sightLimit = 30 * size2;
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (texture != null) {
			var r = getRotation();
			var flip = true;
			if (r < 90 || r > 270) {
				flip = !flip;
			}
			batch.draw(texture, getX(), getY(), // position
					getOriginX(), getOriginY(), // origin
					getWidth(), getHeight(), // size
					getScaleX(), getScaleY(), // scale
					r, // rotation
					0, 0, // texture offset
					texture.getWidth(), texture.getHeight(), // texture size
					true, flip); // flip
		}
	}

	@Override
	public void act(float deltaTime) {
//		var v0 = localToStageCoordinates(new Vector2(getX(Align.bottomLeft)+dx, getY(Align.bottomLeft)+dy));
//		var v1 = localToStageCoordinates(new Vector2(getX(Align.topRight)+dx, getY(Align.topRight)+dy));
		var d = new Vector2(direction).scl(deltaTime * MAX_SPEED);
		// d.scl(0);
		var v0 = getParent()
				.localToStageCoordinates(new Vector2(getX(Align.bottomLeft) + d.x, getY(Align.bottomLeft) + d.y));
		var v1 = getParent()
				.localToStageCoordinates(new Vector2(getX(Align.topRight) + d.x, getY(Align.topRight) + d.y));
		var tmp = new Vector2(direction);
		var borderX = getStage().getWidth() * BORDER;
		var borderY = getStage().getHeight() * BORDER;
		v0.sub(borderX * .5f, borderY * 2);
		v0.x = Math.max(0, -v0.x) / borderX;
		v0.y = Math.max(0, -v0.y) / (borderY * 2);

		v1.sub(getStage().getWidth() - borderX * .5f, getStage().getHeight() - borderY * .1f);
		v1.x = Math.max(0, v1.x) / borderX;
		v1.y = Math.max(0, v1.y) / borderY;
		var wallDist = v0.x + v0.y + v1.x + v1.y;
		// wallDist = 2f - 5*wallDist;
		// wallDist *= wallDist;
		// if (this == FlockingApp.gator)
		// System.out.println(v0 + " " + v1 + " " + wallDist);
		if (wallDist > 0)
			speed = Interpolation.linear.apply(speed, MIN_SPEED, .05f * wallDist);
		else
			speed = Interpolation.linear.apply(speed, MAX_SPEED, .01f);
		// speed = (8*speed + 2 * wallDist*speed) / 10f;
		direction.add(v0.scl(v0).scl(.5f)).sub(v1.scl(v1).scl(.5f)).nor();
		d.set(direction).scl(deltaTime * speed);
		moveBy(d.x, d.y);
		v0.set(getX(Align.center), getY(Align.center));
		v1.set(v0);
		d.set(direction);
		var avgSpeed = speed;
		int count = 1;
		for (Actor a : getParent().getChildren()) {
			if (a == this || !(a instanceof Bird))
				continue;
			Bird other = (Bird) a;
			tmp.set(a.getX(Align.center), a.getY(Align.center));
			var distance = v0.dst2(tmp);
			if (distance > sightLimit)
				continue;
			v1.add(tmp);
			d.add(new Vector2(other.direction).scl((sightLimit - distance) / sightLimit));
			avgSpeed += other.speed;
			count++;
			
//			var directionAway = new Vector2(v0);
//			directionAway.sub(tmp);
//			directionAway.nor();
//			
			var r = new Rectangle(0,0,50,50);
			r.x = 100;
			
			var directionAway = new Vector2(v0).sub(tmp).nor();
			if (distance > 0) {
				// directionAway.rotateDeg(180);
			}
			// if (a == HelloWorld.gator)
			// System.out.println("dist=" + distance + ", size=" + size2 + ", angle=" +
			// direction + ", v=" + velocity + ", s=" + speed);

			if (distance < avoidThreshold) {
				var factor = (avoidThreshold - Math.max(0f, distance)) / avoidThreshold;
				direction.lerp(directionAway, factor * factor / 3f).nor();
			}

//			if(distance < 5*5*size) {
//				direction.lerp(new Vector2(other.velocity).nor(), .2f);
//			}

			// Vector2 v = this.
		}
		direction.lerp(new Vector2(Math.signum(direction.x), 0), .005f);
		if (count > 1) {
			v1.scl(1f / count);
			var directionToCenter = new Vector2(v1).sub(v0).nor();
			d.nor();
			direction.lerp(directionToCenter, 0.05f).nor();
			direction.lerp(d, .1f);
			if (FlockingApp.target.x != 0f || FlockingApp.target.y != 0f) {
				tmp.set(FlockingApp.target).sub(v0);
				var maxLen2 = getStage().getWidth() * getStage().getHeight();
				var distFactor = Math.max(0, (maxLen2 - tmp.len2()) / maxLen2);
				var directionToTarget = tmp.nor();
				direction.lerp(directionToTarget, 0.5f * distFactor).nor();
			}

		}
		avgSpeed /= count;
		speed = (60 * speed + 40 * avgSpeed) / 100f;
		setRotation((direction.angleDeg() + 180) % 360f);
	}

	public void setVelocity(float dx, float dy) {
		if (dx != 0f && dy != 0f) {
			direction.set(dx, dy);
			speed = direction.len();
			direction.nor();
		}
	}
}
