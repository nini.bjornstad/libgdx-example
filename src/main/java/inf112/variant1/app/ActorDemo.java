package inf112.variant1.app;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

public class ActorDemo {
    public static void main(String[] args) {
        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setTitle("Actor Demo");
        cfg.setWindowedMode(1920, 1080);

        new Lwjgl3Application(new ActorApp(), cfg);
    }
}