package inf112.variant1.app;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Align;

import inf112.util.Calculations;

public class Gator extends Actor {
	Texture texture;
	Vector2 direction = new Vector2(1, 0);
	float speed = 500;
	float size2, size;
	float avoidThreshold, sightLimit;
	public float health;

	public Gator(Texture tex, float width, float height) {
		texture = tex;
		setBounds(0, 0, width, height);
		setOrigin(Align.center);
		setPosition(0, 0, Align.bottomLeft);
		setScale(1);

		this.addListener(new InputListener() {

			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				System.out.println("down" + x + ", " + y + " ");
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				System.out.println("up");
			}
		});
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (texture != null) {
			var r = getRotation();
			var flip = true;
			if (r < 90 || r > 270) {
				flip = !flip;
			}
			batch.draw(texture, getX(), getY(), // position
					getOriginX(), getOriginY(), // origin
					getWidth(), getHeight(), // size
					getScaleX(), getScaleY(), // scale
					r, // rotation
					0, 0, // texture offset
					texture.getWidth(), texture.getHeight(), // texture size
					false, flip); // flip
		}
	}

	@Override
	public void act(float deltaTime) {
		
		// flip direction if moving will take us outside screen border
		Vector2 outside = Calculations.checkBorder(this, direction, speed*deltaTime);

		if(outside.x != 0f)
			direction.scl(-1, 1);
		if(outside.y != 0f)
			direction.scl(1, -1);

		setPosition(getX() + direction.x*deltaTime*speed, //
				getY() + direction.y*deltaTime*speed);
		
		outside = Calculations.checkBorder(this);
		setPosition(getX() + outside.x, //
				getY() + outside.y);
		
		for(var child : getParent().getChildren()) {
			if(child == this)
				continue;
			
			if(child instanceof Fish) {
				((Fish) child).kill();
				health += 5f;
			}
		}
	}

	public void setDirection(float dx, float dy) {
		if (dx != 0f || dy != 0f) {
			direction.set(dx, dy).nor();
		}
	}
	
	public void setVelocity(float dx, float dy) {
		if (dx != 0f || dy != 0f) {
			direction.set(dx, dy);
			speed = direction.len();
			direction.nor();
		}
	}

	public float getHealth() {
		return health;
	}
}
